using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductUI : MonoBehaviour
{
    public Button accessProductUIBtn;
    public Text nameProduct;
    public Text price;
    public Image frontImage;
    public Entities.Product product;
    public List<Sprite> spritesProduct;

    public void SetData(Entities.Product product, List<Sprite> spritesProduct)
    {
        this.product = product;
        this.spritesProduct = spritesProduct;
        nameProduct.text = product.name;
        price.text = product.price;
        frontImage.sprite = spritesProduct[0];
    }
}
