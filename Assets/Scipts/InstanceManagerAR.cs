using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class InstanceManagerAR : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject CanvasPlacer;
    public GameObject cursorObject;
    public GameObject prefabObject;
    public ARRaycastManager raycastManager;
    public delegate void Placed(MarketObject obj);
    public event Placed OnPlaced;


    public Button instanceObjBtn;
    public Button cancelPlacementBtn;

    public List<GameObject> stands;

    //[SerializeField] bool useCursor = true;

    // Start is called before the first frame update

    public void ActivePlacer(GameObject objectPlace)
    {
        prefabObject = objectPlace;
        CanvasPlacer.SetActive(true);
    }
    private void Start()
    {
        //cursorObject.SetActive(useCursor);
        instanceObjBtn.onClick.AddListener(() =>
        {
            GameObject temp = Instantiate(prefabObject.gameObject, cursorObject.transform.position, cursorObject.transform.rotation);
            stands.Add(temp);
            OnPlaced?.Invoke(temp.GetComponent<MarketObject>());
            CanvasPlacer.gameObject.SetActive(false);
            
        });

        cancelPlacementBtn.onClick.AddListener(() =>
        {
            CanvasPlacer.gameObject.SetActive(false);
        });
    }

    private void Update()
    {
        UpdateCursor();
    }

    private void UpdateCursor()
    {
        Vector2 screenPosition = mainCamera.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));

        List<ARRaycastHit> hits = new List<ARRaycastHit>();

        raycastManager.Raycast(screenPosition, hits, TrackableType.Planes);

        if (hits.Count > 0)
        {
            cursorObject.transform.position = hits[0].pose.position;
            cursorObject.transform.rotation = hits[0].pose.rotation;
        }
    }
}
