using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] InstanceManagerAR instanceManagerAR;

    public MarketObject MarketStand;

    [Header("Genearl UI")]
    public GameObject LoadingGeneral;
    public List<Entities.Seller> sellers;

    [Header("Placer UI")]
    public List<SellerUI> sellerUIs;


    [Header("SellerUI panel")]
    public GameObject SellerUIPanel;
    public GameObject SellerUIPanelLoadingEffect;
    public Transform SellerUIContainer;
    public Button AccessSellerUIPanelBtn;
    public Button ExitSellerPanelBtn;
    public bool SellersReady;


    [Header("Market UI")]
    public GameObject MarketUIPanel;
    public Transform ProductUIContainer;
    public GameObject DownloadingEffect;
    public Button ExitMarketUIBtn;

    [Header("Product UI")]
    public GameObject ProductUIPanel;
    public Text NameProduct;
    public Text DescriptionProduct;
    public Text PriceProduct;
    public List<Image> imagesProduct;
    public Button WebProductAccessBtn;
    public Button ExitProductUI;

    private Entities.Seller currentSeller;


    // Start is called before the first frame update
    void Start()
    {
        LoadingGeneral.SetActive(true);

        NodeServices.GetSellers((sellers) =>
        {
            this.sellers = sellers;
            SellersReady = true;
            SellerUI_Init();
            LoadingGeneral.SetActive(false);
        },
        (error) =>
        {
            Debug.Log(error);
        });

        ExitProductUI.onClick.AddListener(()=>
        {
            ProductUIPanel.SetActive(false);
        });

        ExitMarketUIBtn.onClick.AddListener(() =>
        {
            MarketUIPanel.SetActive(false);
        });

    }

    private void SellerUI_Init()
    {
        
        AccessSellerUIPanelBtn.onClick.AddListener(() =>
        {
            InstanceSellerUIs();
            SellerUIPanel.SetActive(true);
        });

        ExitSellerPanelBtn.onClick.AddListener(() =>
        {
            SellerUIPanel.SetActive(false);
        });

        instanceManagerAR.OnPlaced += (obj) =>
          {
              Debug.Log(obj + " ah sido colocado en escena");
              obj.SetData(currentSeller,instanceManagerAR.mainCamera);
              obj.OpenMarketBtn.onClick.AddListener(() =>
              {
                  OpenMarketUI(obj.products);
              });
          };


    }

    private void InstanceSellerUIs()
    {
        SellerUIPanelLoadingEffect.SetActive(true);
        ClearSellersUI();

        foreach (var item in sellers)
        {
            SellerUI sellerUI = Instantiate(SellerUIContainer.GetChild(0).gameObject, SellerUIContainer).GetComponent<SellerUI>();
            sellerUI.gameObject.SetActive(true);
            sellerUI.Set(item);
            sellerUI.sellerUIPlacerBtn.onClick.AddListener(() =>
            {
                PlaceMarket(sellerUI.seller);
                Destroy(sellerUI.gameObject);
                SellerUIPanel.SetActive(false);
            });
        }
        SellerUIPanelLoadingEffect.SetActive(false);
    }



    private void ClearSellersUI()
    {

        if (SellerUIContainer.childCount > 1)
            for (var i = 1; i < SellerUIContainer.childCount; i++)
            {
                Destroy(SellerUIContainer.GetChild(i).gameObject);
            }


    }

    private void PlaceMarket(Entities.Seller seller)
    {
        instanceManagerAR.ActivePlacer(MarketStand.gameObject);
        currentSeller=seller;

    }



    private void ClearMarketUI()
    {
        if (ProductUIContainer.childCount > 1)
            for (var i = 1; i < ProductUIContainer.childCount; i++)
            {
                Destroy(ProductUIContainer.GetChild(i).gameObject);
            }
    }



    public void OpenMarketUI(List<Entities.Product> products)
    {
        ClearMarketUI();
        MarketUIPanel.SetActive(true);
        foreach (var item in products)
        {
            ProductUI productUI = Instantiate(ProductUIContainer.GetChild(0).gameObject, ProductUIContainer).GetComponent<ProductUI>();
            productUI.gameObject.SetActive(true);
            productUI.SetData(item,item.spritesProduct);

            productUI.accessProductUIBtn.onClick.AddListener(()=>
            {
                OpenProductUIPanel(item,item.spritesProduct);
            });
        }

    }

    private void OpenProductUIPanel(Entities.Product product,List<Sprite> sprites)
    {
        ProductUIPanel.SetActive(true);
        foreach (var item in imagesProduct)
        {
            item.gameObject.SetActive(false);
        }
        for (var i = 0; i < sprites.Count; i++)
        {
            imagesProduct[i].sprite = sprites[i];
            imagesProduct[i].gameObject.SetActive(true);
        }

        NameProduct.text = product.name;
        PriceProduct.text = "S/."+product.price;
        DescriptionProduct.text = product.description;
        
        WebProductAccessBtn.onClick.RemoveAllListeners();
        WebProductAccessBtn.onClick.AddListener(()=>
        {
            OpenStoreWeb(product.linkStore);
        });
    }

    private void OpenStoreWeb(string url)
    {
        Tool.OpenWebSite(url);
    }
}
