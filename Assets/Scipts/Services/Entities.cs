using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entities
{
    [System.Serializable]
    public class SellerArrayDAO
    {
        public string[] idsellerArray;
        public SellerDAO[] sellerArray;

        public void Print()
        {
            for (var i = 0; i < idsellerArray.Length; i++)
            {
                Debug.Log(idsellerArray[i] + ": " + sellerArray[i].ToArrayObj());
            }
        }

        public List<Seller> GetSellers()
        {
            List<Seller> sellers = new List<Seller>();
            for (var i = 0; i < idsellerArray.Length; i++)
            {
                Seller seller = new Seller(idsellerArray[i], sellerArray[i].contact, sellerArray[i].email, sellerArray[i].name);
                sellers.Add(seller);
            }

            return sellers;
        }
    }

    [System.Serializable]
    public class SellerDAO
    {
        public string dni;
        public string name;
        public string contact;
        public string email;
        public string password;

        public string ToArrayObj()
        {
            return dni + " " + name + " " + contact + " " + email;
        }
    }

    [System.Serializable]
    public class Seller
    {
        public string id;
        public string cellphone;
        public string email;
        public string name;

        public Seller(string id, string cellphone, string email, string name)
        {
            this.id = id;
            this.cellphone = cellphone;
            this.email = email;
            this.name = name;
        }
    }

    [System.Serializable]
    public class ProductsArrayDAO
    {
        //public string[] idProductsArray;
        public ProductDAO[] productArray;

        // public void Print()
        // {
        //     for (var i = 0; i < idProductsArray.Length; i++)
        //     {
        //         Debug.Log(idProductsArray[i] + ": " + productArray[i].ToArrayObj());
        //     }
        // }

        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            for (var i = 0; i < productArray.Length; i++)
            {
                Product product = new Product(productArray[i].id, productArray[i].name, productArray[i].price, productArray[i].description, productArray[i].id_photo, productArray[i].link);
                products.Add(product);
            }

            return products;
        }
    }


    [System.Serializable]
    public class ProductDAO
    {
        public string id;
        public string name;
        public string price;
        public string description;
        public string id_photo;
        public string link;

        public string ToArrayObj()
        {
            return name + " " + price + " " + description + " " + id_photo + " " + link;
        }
    }

    [System.Serializable]
    public class Product
    {
        public string id;
        public string name;
        public string price;
        public string description;
        public string idPhoto;
        public string linkStore;
        public GalleryProduct galleryProduct;
        public List<Sprite> spritesProduct = new List<Sprite>();

        public Product(string id, string name, string price, string description, string idPhoto, string linkStore)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.description = description;
            this.idPhoto = idPhoto;
            this.linkStore = linkStore;
        }

        public string ToStringObj()
        {
            return id + " " + name + " " + price + " " + description + " " + idPhoto + " " + linkStore;
        }
    }

    [System.Serializable]
    public class GalleryArrayDAO
    {
        public string[] idGalleryArray;
        public GalleryDAO[] galleryArray;

        public void Print()
        {
            for (var i = 0; i < idGalleryArray.Length; i++)
            {
                Debug.Log(idGalleryArray[i] + ":\n " + galleryArray[i].ToArrayObj());
            }
        }

        public List<GalleryProduct> GetGalleryProducts()
        {
            List<GalleryProduct> galleryProducts = new List<GalleryProduct>();
            for (var i = 0; i < idGalleryArray.Length; i++)
            {
                List<string> urls = new List<string>();
                urls.Add(galleryArray[i].url1);
                if(galleryArray[i].url2!=null)
                urls.Add(galleryArray[i].url2);
                if(galleryArray[i].url3!=null)
                urls.Add(galleryArray[i].url3);
                GalleryProduct galleryProduct = new GalleryProduct(idGalleryArray[i],urls);
                galleryProducts.Add(galleryProduct);
            }

            return galleryProducts;
        }
    }

    [System.Serializable]
    public class GalleryDAO
    {
        public string url1;
        public string url2;
        public string url3;

        public string ToArrayObj()
        {
            return url1 + "\n" + url2 + "\n" + url3;
        }
    }

    [System.Serializable]
    public class GalleryProduct
    {
        public string idGallery;
        public List<string> urls;

        public GalleryProduct(string idGallery, List<string> urls)
        {
            this.idGallery = idGallery;
            this.urls = urls;
        }
    }


    
}
