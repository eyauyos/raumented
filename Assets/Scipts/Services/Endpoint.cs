using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Endpoint
{
    public const string GET_SELLERS= "https://raumented.herokuapp.com/api/getSellers/";
    public const string GET_PRODUCTS= "https://raumented.herokuapp.com/api/getProducts/"; //ADD ID_SELLER FOR REQ
    public const string GET_GALLERIES= "https://raumented.herokuapp.com/api/getPhotos/"; //ADD ID_SELLER FOR REQ

}
