using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class NodeServices
{

    public static void GetSellers(UnityAction<List<Entities.Seller>> sucess, UnityAction<string> error)
    {
        string uri = Endpoint.GET_SELLERS;
        UnityWebRequest www = UnityWebRequest.Get(uri);

        var request = www.SendWebRequest();
        string[] pages = uri.Split('/');
        int page = pages.Length - 1;

        request.completed += (b) =>
            {

                switch (www.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        //Debug.LogError(pages[page] + ": Error: " + www.error);
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        //Debug.LogError(pages[page] + ": HTTP Error: " + www.error);
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.Success:
                        Debug.Log(pages[page] + ":\nReceived: " + www.downloadHandler.text);
                        Entities.SellerArrayDAO temp = JsonUtility.FromJson<Entities.SellerArrayDAO>(www.downloadHandler.text);
                        sucess?.Invoke(temp.GetSellers());
                        break;
                }
            };
    }
    public static void GetProductsFromSeller(string idSeller, UnityAction<List<Entities.Product>> sucess, UnityAction<string> error)
    {
        string uri = Endpoint.GET_PRODUCTS + idSeller;
        UnityWebRequest www = UnityWebRequest.Get(uri);

        var request = www.SendWebRequest();
        string[] pages = uri.Split('/');
        int page = pages.Length - 1;

        request.completed += (b) =>
            {

                switch (www.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        //Debug.LogError(pages[page] + ": Error: " + www.error);
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        //Debug.LogError(pages[page] + ": HTTP Error: " + www.error);
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.Success:
                        Debug.Log(pages[page] + ":\nReceived: " + www.downloadHandler.text);
                        Entities.ProductsArrayDAO temp = JsonUtility.FromJson<Entities.ProductsArrayDAO>(www.downloadHandler.text);
                        sucess?.Invoke(temp.GetProducts());
                        break;
                }
            };
    }

    public static void GetGalleriesFromSeller(string idSeller, UnityAction<List<Entities.GalleryProduct>> sucess, UnityAction<string> error)
    {
        string uri = Endpoint.GET_GALLERIES + idSeller;
        UnityWebRequest www = UnityWebRequest.Get(uri);

        var request = www.SendWebRequest();
        string[] pages = uri.Split('/');
        int page = pages.Length - 1;

        request.completed += (b) =>
            {

                switch (www.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        //Debug.LogError(pages[page] + ": Error: " + www.error);
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        //Debug.LogError(pages[page] + ": HTTP Error: " + www.error);
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.Success:
                        Debug.Log(pages[page] + ":\nReceived: " + www.downloadHandler.text);
                        Entities.GalleryArrayDAO temp = JsonUtility.FromJson<Entities.GalleryArrayDAO>(www.downloadHandler.text);
                        sucess?.Invoke(temp.GetGalleryProducts());
                        break;
                }
            };
    }

    


    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    break;
            }
        }
    }
}
