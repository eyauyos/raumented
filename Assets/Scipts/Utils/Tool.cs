﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class Tool
{
    public static void OpenWebSite(string url)
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        Application.ExternalEval("window.open(url);");
#else
        Application.OpenURL(url);
#endif
    }

    public static Sprite CreateSpriteFromTexture2D(Texture2D texture2D)
    {
        return Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100.0f);
    }


    public static void GetTexture(string uri, UnityAction<Texture2D> sucess, UnityAction<string> error)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(uri);
        var request = www.SendWebRequest();
        string[] pages = uri.Split('/');
        int page = pages.Length - 1;
        request.completed += (a) =>
          {
            //   if (www.result != UnityWebRequest.Result.Success)
            //   {
            //       Debug.Log(www.error);
            //   }
            //   else
            //   {
            //       Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            //   }

              switch (www.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        error?.Invoke(www.error);
                        break;
                    case UnityWebRequest.Result.Success:
                        Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                        sucess?.Invoke(myTexture);
                        break;
                }
          };

    }
}
