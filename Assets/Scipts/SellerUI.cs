using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellerUI : MonoBehaviour
{
    public Button sellerUIPlacerBtn;
    public Text nameSellerMarket;
    public Entities.Seller seller;

    public void Set(Entities.Seller seller)
    {
        this.seller = seller;
        nameSellerMarket.text = this.seller.name;
    }

}
