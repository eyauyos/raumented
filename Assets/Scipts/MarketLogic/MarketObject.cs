using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class MarketObject : MonoBehaviour
{
    public Canvas canvas;
    public GameObject LoadingPanel;
    public TextMeshProUGUI TittleMarket;
    public Button OpenMarketBtn;
    public Button DestroyMarketBtn;
    public Entities.Seller seller;
    public List<Entities.Product> products = new List<Entities.Product>();
    public List<Entities.GalleryProduct> galleryProducts = new List<Entities.GalleryProduct>();

    public int progress;

    void Start()
    {
        LoadingPanel.SetActive(true);

        GetProducts(() =>
        {
            GetGalleryProducts(() =>
            {
                SetSpritesProducts();
                SetListSritesToProducts(()=>
                {
                    LoadingPanel.SetActive(false);
                });
            });

        });

        DestroyMarketBtn.onClick.AddListener(()=>
        {
            Destroy(gameObject);
        });
    }

    public void SetData(Entities.Seller seller, Camera mainCamera)
    {
        canvas.worldCamera = mainCamera;
        this.seller = seller;
        TittleMarket.text = "Tienda de "+seller.name;
    }

    private void GetProducts(UnityAction sucess)
    {
        NodeServices.GetProductsFromSeller(seller.id, (listProducts) =>
         {
             products = listProducts;
             sucess?.Invoke();
         },
        (error) =>
        {
            Debug.Log("error");
        });
    }

    private void GetGalleryProducts(UnityAction sucess)
    {
        NodeServices.GetGalleriesFromSeller(seller.id, (listGalleries) =>
         {
             galleryProducts = listGalleries;
             sucess?.Invoke();
         },
        (error) =>
        {
            Debug.Log("error");
        });
    }

    private void SetSpritesProducts()
    {
        foreach (var g in galleryProducts)
        {
            foreach (var p in products)
            {
                if (p.id == g.idGallery)
                {
                    p.galleryProduct = g;
                }
            }
        }
    }

    private void SetListSritesToProducts(UnityAction readyData)
    {
        UnityAction<int> readyDownload = null;
        int countDownload = products.Count;
        for (var i = 0; i < products.Count; i++)
        {
            GetProductSpritesFromServerGallery(products[i], () =>
             {
                 countDownload++;
                 readyDownload?.Invoke(countDownload);
             });
        }

        readyDownload += (countDload) =>
          {
              progress = (countDownload/products.Count);
              if(progress==1)
              {
                  readyData?.Invoke();
              }

          };
    }



    private void GetProductSpritesFromServerGallery(Entities.Product product, UnityAction readyData)
    {
        UnityAction<int> readyDownload = null;
        Texture2D firstTexture = null;
        Texture2D secondTexture = null;
        Texture2D thirdTexture = null;
        int count = 0;

        Tool.GetTexture(product.galleryProduct.urls[0], (texture) =>
         {
             firstTexture = texture;
             count++;
             readyDownload?.Invoke(count);
         },
        (error) =>
        {
            Debug.Log(error);
        });

        if(product.galleryProduct.urls.Count>1)
        Tool.GetTexture(product.galleryProduct.urls[1], (texture) =>
         {
             secondTexture = texture;
             count++;
             readyDownload?.Invoke(count);
         },
        (error) =>
        {
            Debug.Log(error);
        });

        if(product.galleryProduct.urls.Count>2)
        Tool.GetTexture(product.galleryProduct.urls[2], (texture) =>
         {
             thirdTexture = texture;
             count++;
             readyDownload?.Invoke(count);
         },
        (error) =>
        {
            Debug.Log(error);
        });

        readyDownload += (readyCount) =>
          {
              switch (readyCount)
              {
                  case 1:
                      Debug.Log("primera imagen descargada");
                      break;
                  case 2:
                      Debug.Log("segunda imagen descargada");
                      break;
                  case 3:
                      Debug.Log("segunda imagen descargada");
                      break;
              }

              if (readyCount == product.galleryProduct.urls.Count)
              {

                  product.spritesProduct.Add(Tool.CreateSpriteFromTexture2D(firstTexture));
                  if(secondTexture!=null)
                  product.spritesProduct.Add(Tool.CreateSpriteFromTexture2D(secondTexture));
                  if(thirdTexture!=null)
                  product.spritesProduct.Add(Tool.CreateSpriteFromTexture2D(thirdTexture));

                  readyData?.Invoke();
              }
          };
    }
}
